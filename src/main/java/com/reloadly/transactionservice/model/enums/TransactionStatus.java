package com.reloadly.transactionservice.model.enums;

public enum TransactionStatus {
    SUCCESS, PENDING, IN_PROGRESS, FAILED
}