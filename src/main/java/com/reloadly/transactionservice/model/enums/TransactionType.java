package com.reloadly.transactionservice.model.enums;

public enum TransactionType {
    DEBIT, CREDIT
}