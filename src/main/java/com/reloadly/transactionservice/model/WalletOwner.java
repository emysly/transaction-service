package com.reloadly.transactionservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "wallet_owner")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WalletOwner extends BaseEntity {

    @NotEmpty
    private String clientId;

    @NotEmpty
    private String phoneNumber;

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    private String firstName;

    private String middleName;

    @NotEmpty
    private String lastName;

    private String fullName = firstName + " " + middleName + " " + lastName;

    @Pattern(regexp = "^\\d{10,}$")
    private String bvn;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime dateOfBirth;

    private Gender gender;

    private MaritalStatus maritalStatus;

    private String employerName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    Date employmentDate;

    public static enum MaritalStatus {
        Single,
        Married,
        Widowed,
        Divorced,
        Separated
    }

    public static enum Gender {
        MALE,
        FEMALE,
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        WalletOwner that = (WalletOwner) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
