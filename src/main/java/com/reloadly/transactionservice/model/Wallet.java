package com.reloadly.transactionservice.model;

import com.reloadly.transactionservice.model.enums.ActivityType;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import java.util.Objects;

@Entity
@Table(name = "wallet")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Wallet extends BaseEntity {

    @ManyToOne
    @JoinColumn(nullable = false)
    private WalletOwner walletOwner;
    @Column(nullable = false)
    @NotEmpty
    private String walletId;
    @Column(nullable = false)
    @NotEmpty
    private String clientId;
    @Column(nullable = false)
    @NotEmpty
    String country;
    @Column
    @PositiveOrZero
    Long balance = 0L;
    @Column
    private boolean locked = false;
    @Column
    private String reason;
    @Column
    @Enumerated(EnumType.STRING)
    private ActivityType lockType = ActivityType.UNLOCK;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Wallet wallet = (Wallet) o;
        return getId() != null && Objects.equals(getId(), wallet.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
