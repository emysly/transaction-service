package com.reloadly.transactionservice.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "user")
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiUser extends BaseEntity {
    private String userName;
    private String password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ApiUser apiUser = (ApiUser) o;
        return getId() != null && Objects.equals(getId(), apiUser.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
