package com.reloadly.transactionservice.model;

import com.reloadly.transactionservice.model.enums.TransactionStatus;
import com.reloadly.transactionservice.model.enums.TransactionType;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "transaction")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction extends BaseEntity {

    @ManyToOne
    @JoinColumn(nullable = false)
    private Wallet wallet;

    @Column(nullable = false)
    private long amount;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String uniqueRef;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus = TransactionStatus.PENDING;

    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;

    private String errorMessage;

    void ensureValuesBeforeCreate() {
        ensureValues();
    }

    void ensureValues() {
        if (transactionType == TransactionType.DEBIT && amount >= 0) {
            throw new IllegalStateException("Debit can only have negative amount");
        }

        if (transactionType == TransactionType.CREDIT && amount < 0) {
            throw new IllegalStateException("Credit can only have positive amount");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Transaction that = (Transaction) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}