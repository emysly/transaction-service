package com.reloadly.transactionservice.repository;

import com.reloadly.transactionservice.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    int countByUniqueRef(String uniqueRef);

    Transaction getTransactionByUniqueRef(String uniqueRef);

    List<Transaction> findTransactionByWalletIdAndCreatedAtBetween(Long id, Date startDate, Date endDate);

}
