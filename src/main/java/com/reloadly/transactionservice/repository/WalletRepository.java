package com.reloadly.transactionservice.repository;

import com.reloadly.transactionservice.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    Optional<Wallet> getWalletByWalletId(String walletId);

    Optional<Wallet> getWalletByClientIdAndCountry(String clientId, String countryCode);

    Optional<Wallet> getWalletByWalletIdAndCountry(String walletId, String countryCode);

    Wallet findOneByWalletIdAndCountry(String walletId, String country);
}
