package com.reloadly.transactionservice.repository;

import com.reloadly.transactionservice.model.ApiUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiUserRepository extends JpaRepository<ApiUser, Long> {
    ApiUser findByUserName(String username);
}
