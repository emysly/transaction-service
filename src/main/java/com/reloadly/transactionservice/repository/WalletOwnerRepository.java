package com.reloadly.transactionservice.repository;

import com.reloadly.transactionservice.model.WalletOwner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WalletOwnerRepository extends JpaRepository<WalletOwner, Long> {
    Optional<WalletOwner> getWalletOwnerByClientId(String clientId);
}
