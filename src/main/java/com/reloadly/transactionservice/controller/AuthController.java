package com.reloadly.transactionservice.controller;

import com.reloadly.transactionservice.exception.AuthenticationException;
import com.reloadly.transactionservice.http.AuthRequest;
import com.reloadly.transactionservice.http.AuthResponse;
import com.reloadly.transactionservice.service.utils.jwt.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Slf4j
public class AuthController {

    private final JwtUtil jwtUtil;
    private final AuthenticationManager authenticationManager;

    @PostMapping
    public ResponseEntity<AuthResponse> generateToken(@Valid @RequestBody AuthRequest authRequest) {
        log.info("Here..");
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        } catch (Exception ex) {
            log.error("Auth Error: {}", ex.getMessage());
            throw new AuthenticationException("Invalid Username or Password");
        }
        String token = jwtUtil.generateToken(authRequest.getUsername());
        AuthResponse response = AuthResponse.builder().username(authRequest.getUsername()).token(token).build();
        return ResponseEntity.ok(response);
    }
}
