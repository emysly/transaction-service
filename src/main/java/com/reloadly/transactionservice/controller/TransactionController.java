package com.reloadly.transactionservice.controller;

import com.reloadly.transactionservice.filters.EntityCodeSetter;
import com.reloadly.transactionservice.http.ApiResponse;
import com.reloadly.transactionservice.http.CrossWalletRequest;
import com.reloadly.transactionservice.http.TransactionRequest;
import com.reloadly.transactionservice.service.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "api/v1/wallet")
@RequiredArgsConstructor
@Slf4j
public class TransactionController {

    private final TransactionService transactionService;

    @PostMapping(value = "/{walletId}/credit")
    public ApiResponse creditFunds(@PathVariable String walletId, @Valid @RequestBody TransactionRequest transactionRequest) {
        log.info("WALLET_CREDIT to account : {} for amount: {}", walletId, transactionRequest.getAmount());
        return transactionService.creditFunds(walletId, transactionRequest, EntityCodeSetter.getRequestEntity());
    }

    @PostMapping(value = "/{walletId}/debit")
    public ApiResponse debitFunds(@PathVariable String walletId, @Valid @RequestBody TransactionRequest transactionRequest) {
        log.info("WALLET_DEBIT on account : {} for amount: {}", walletId, transactionRequest.getAmount());
        return transactionService.debitFunds(walletId, transactionRequest, EntityCodeSetter.getRequestEntity());
    }

    @PostMapping(value = "/transfer")
    public ApiResponse transferFunds(@Valid @RequestBody CrossWalletRequest crossWalletRequest) {
        log.info("WALLET_INTERNAL_TRANSFER from {} to {}, amount {}", crossWalletRequest.getSourceAccountId(),
                crossWalletRequest.getTargetAccountId(), crossWalletRequest.getAmount());
        return transactionService.transferFund(crossWalletRequest, EntityCodeSetter.getRequestEntity());
    }

}
