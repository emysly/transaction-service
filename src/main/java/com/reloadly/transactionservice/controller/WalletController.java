package com.reloadly.transactionservice.controller;

import com.reloadly.transactionservice.filters.EntityCodeSetter;
import com.reloadly.transactionservice.http.ApiResponse;
import com.reloadly.transactionservice.http.WalletRequest;
import com.reloadly.transactionservice.service.WalletService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;

@RestController
@RequestMapping(value = "api/v1/wallet")
@RequiredArgsConstructor
@Slf4j
public class WalletController {

    private final WalletService walletService;

    @PostMapping(value = "/create")
    public ApiResponse createWallet(@Valid @RequestBody WalletRequest walletRequest) {
        log.info("WALLET_CREATION with client ID: {}}", walletRequest.getClientId());
        return walletService.createWallet(walletRequest, EntityCodeSetter.getRequestEntity());
    }

    @GetMapping(value = "/{walletId}")
    public ApiResponse getWallet(@PathVariable String walletId) {
        return walletService.getWallet(walletId, EntityCodeSetter.getRequestEntity());
    }

    @GetMapping(value = "/client-id/{clientId}")
    public ApiResponse getWalletByClientId(@PathVariable String clientId) {
        return walletService.getWalletByClientId(clientId, EntityCodeSetter.getRequestEntity());
    }

    @GetMapping(value = "/{walletId}/transactions")
    public ApiResponse getTransactionHistory(@PathVariable String walletId, @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date startDate, @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") Date endDate) {
        log.info("WalletId: {} - StartDate: {} - EndDate: {}", walletId, startDate, endDate);
        return walletService.getTransactions(walletId, startDate, endDate, EntityCodeSetter.getRequestEntity());
    }

    @PostMapping(value = "/{walletId}/lock")
    public ApiResponse lock(@PathVariable String walletId, @RequestParam String reason) {
        log.info(walletId, String.format("WALLET_LOCK with reason: %s", reason));
        return walletService.lockWallet(walletId, reason, EntityCodeSetter.getRequestEntity());
    }

    @PostMapping(value = "/{walletId}/unlock")
    public ApiResponse unlock(@PathVariable String walletId, @RequestParam String reason) {
        log.info("{} WALLET_UNLOCK with reason: {}", walletId, reason);
        return walletService.unlockWallet(walletId, reason, "NG");
    }
}
