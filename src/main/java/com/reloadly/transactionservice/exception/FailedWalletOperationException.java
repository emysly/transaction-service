package com.reloadly.transactionservice.exception;

abstract public class FailedWalletOperationException extends WalletException {

    public FailedWalletOperationException() {
    }

    public FailedWalletOperationException(String message) {
        super(message);
    }

    public FailedWalletOperationException(String message, boolean clientError) {
        super(message, clientError);
    }

    public FailedWalletOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailedWalletOperationException(Throwable cause) {
        super(cause);
    }

    @Override
    public boolean isClientError() {
        return true;
    }
    
}