package com.reloadly.transactionservice.exception;

public class MailException extends BaseException {
    public MailException() {
        super();
    }

    public MailException(String message) {
        super(message);
    }

    public MailException(String message, boolean clientError) {
        super(message, clientError);
    }

    public MailException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailException(Throwable cause) {
        super(cause);
    }
}