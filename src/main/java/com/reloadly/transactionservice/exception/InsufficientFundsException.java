package com.reloadly.transactionservice.exception;

public class InsufficientFundsException extends FailedWalletOperationException {

    private void init() {
        clientError = true;
        errorCode = "E02";
        status = 400;
    }

    public InsufficientFundsException() {
        super();
        init();
    }

    public InsufficientFundsException(String msg) {
        super(msg);
        init();

    }

    public InsufficientFundsException(Throwable cause) {
        super(cause);
        init();

    }

    public InsufficientFundsException(String message, Throwable cause) {
        super(message, cause);
        init();

    }
}
