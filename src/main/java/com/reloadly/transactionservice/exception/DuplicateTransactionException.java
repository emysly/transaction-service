package com.reloadly.transactionservice.exception;

public class DuplicateTransactionException extends FailedWalletOperationException {

    public DuplicateTransactionException() {
    }

    public DuplicateTransactionException(String message) {
        super(message);
        setStatus(400);
        setErrorCode("E03");
    }

    public DuplicateTransactionException(String message, boolean clientError) {
        super(message, clientError);
    }

    public DuplicateTransactionException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateTransactionException(Throwable cause) {
        super(cause);
    }

}