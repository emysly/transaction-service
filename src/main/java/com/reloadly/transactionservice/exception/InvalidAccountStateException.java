package com.reloadly.transactionservice.exception;

public class InvalidAccountStateException extends BaseException {
    private void init() {
        errorCode = "E01";
        status = 500;
    }

    public InvalidAccountStateException() {
        super();
        init();
    }

    public InvalidAccountStateException(String msg) {
        super(msg);
        init();

    }

    public InvalidAccountStateException(Throwable cause) {
        super(cause);
        init();

    }

    public InvalidAccountStateException(String message, Throwable cause) {
        super(message, cause);
        init();
    }
}
