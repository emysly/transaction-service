package com.reloadly.transactionservice.exception;

public class AuthenticationException extends BaseException {
    public AuthenticationException(String message) {
        super(message);
        setErrorCode("Unauthorized");
        setStatus(401);
    }
}
