package com.reloadly.transactionservice.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@EqualsAndHashCode(callSuper = false)
@Data
@ToString
public class BaseException extends RuntimeException {

    protected boolean clientError;
    protected int status;
    protected String errorCode;

    public BaseException() {
        super();
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, boolean clientError) {
        super(message);
        this.clientError = clientError;
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

}
