package com.reloadly.transactionservice.exception;

public class WalletException extends BaseException {
    public WalletException() {
    }

    public WalletException(String message) {
        super(message);
    }

    public WalletException(String message, boolean clientError) {
        super(message, clientError);
        status = 400;
    }

    public WalletException(String message, Throwable cause) {
        super(message, cause);
    }

    public WalletException(Throwable cause) {
        super(cause);
    }
}
