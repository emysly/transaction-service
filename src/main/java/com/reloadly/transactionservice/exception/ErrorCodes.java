package com.reloadly.transactionservice.exception;

public class ErrorCodes {
    public static final String INVALID_FIELD = "INVALID_FIELD";
    public static final String MISSING_PATH = "MISSING_PATH";
    public static final String MISSING_REQUEST_PARAM = "MISSING_REQUEST_PARAM";
    public static final String FAILED_CODE = "FAIL";
    public static final String NOT_FOUND_CODE = "NOT_FOUND";
}
