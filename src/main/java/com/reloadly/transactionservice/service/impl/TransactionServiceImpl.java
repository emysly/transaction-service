package com.reloadly.transactionservice.service.impl;

import com.reloadly.transactionservice.api.NotificationApi;
import com.reloadly.transactionservice.exception.BaseException;
import com.reloadly.transactionservice.exception.DuplicateTransactionException;
import com.reloadly.transactionservice.exception.ErrorCodes;
import com.reloadly.transactionservice.exception.FailedWalletOperationException;
import com.reloadly.transactionservice.exception.InsufficientFundsException;
import com.reloadly.transactionservice.exception.InvalidAccountStateException;
import com.reloadly.transactionservice.exception.ResourceNotFoundException;
import com.reloadly.transactionservice.exception.WalletException;
import com.reloadly.transactionservice.http.ApiResponse;
import com.reloadly.transactionservice.http.CrossWalletRequest;
import com.reloadly.transactionservice.http.EmailRequest;
import com.reloadly.transactionservice.http.TransactionRequest;
import com.reloadly.transactionservice.http.TransactionResponse;
import com.reloadly.transactionservice.model.Transaction;
import com.reloadly.transactionservice.model.Wallet;
import com.reloadly.transactionservice.model.enums.TransactionStatus;
import com.reloadly.transactionservice.repository.TransactionRepository;
import com.reloadly.transactionservice.repository.WalletRepository;
import com.reloadly.transactionservice.service.TransactionService;
import com.reloadly.transactionservice.service.utils.TransactionRequestMapper;
import com.reloadly.transactionservice.service.utils.WalletUtils;
import com.reloadly.transactionservice.service.utils.converter.TransactionConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.text.NumberFormat;
import java.util.Optional;

import static com.reloadly.transactionservice.model.enums.TransactionStatus.SUCCESS;
import static com.reloadly.transactionservice.model.enums.TransactionType.CREDIT;
import static com.reloadly.transactionservice.model.enums.TransactionType.DEBIT;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    public static final String SUCCESSFUL_CREDIT_TRANSACTION_SUBJECT = "Successful Credit Transaction";
    public static final String SUCCESSFUL_DEBIT_TRANSACTION_SUBJECT = "Successful Debit Transaction";
    private final WalletRepository walletRepository;
    private final TransactionRepository transactionRepository;
    private final NotificationApi notificationApi;
    private final WalletUtils walletUtils;


    @Override
    public ApiResponse debitFunds(String walletId, TransactionRequest transactionRequest, String countryCode) {

        transactionRequest.setUniqueRef(walletUtils.generateTransactionRef());
        Optional<Wallet> retWallet = walletRepository.getWalletByWalletIdAndCountry(walletId, countryCode);
        if (retWallet.isEmpty()) {
            throw new ResourceNotFoundException("Wallet with specified walletId not found");
        }

        Wallet wallet = retWallet.get();
        if (wallet.isLocked()) {
            throw new InvalidAccountStateException("Wallet is locked for debit.");
        }

        TransactionResponse transactionResponse = TransactionRequestMapper.mapFundsDataToTransaction(transactionRequest, walletId, DEBIT);

        try {
            checkSufficientFunds(wallet, transactionRequest);
            createTransaction(transactionResponse, countryCode);

            transactionResponse = debitAccount(wallet, transactionResponse);
            updateStatus(transactionResponse);
            log.info("Wallet: {} - TransactionResponse: {}", wallet, transactionResponse);

            ApiResponse response = getTransactionResponse(transactionResponse);
            if (response.isSuccess()) {
                String firstName = !isEmpty(wallet.getWalletOwner().getFirstName()) ? capitalize(wallet.getWalletOwner().getFirstName().trim()) : "Anonymous";
                String amount = NumberFormat.getCurrencyInstance().format(transactionRequest.getAmount());
                String email = wallet.getWalletOwner().getEmail().trim();

                EmailRequest emailRequest = EmailRequest.builder().to(email).subject(SUCCESSFUL_DEBIT_TRANSACTION_SUBJECT)
                        .text(walletUtils.generateEmailText(firstName, amount, walletId, "debited from"))
                        .build();
                notificationApi.sendEmail(emailRequest);
            }

            return response;
        } catch (WalletException e) {
            log.error("Error debiting account: {}", e.getMessage());
            return fromException(e, transactionResponse);
        }
    }

    @Override
    public ApiResponse creditFunds(String walletId, TransactionRequest transactionRequest, String countryCode) {
        transactionRequest.setUniqueRef(walletUtils.generateTransactionRef());
        Optional<Wallet> retWallet = walletRepository.getWalletByWalletIdAndCountry(walletId, countryCode);
        if (retWallet.isEmpty()) {
            throw new ResourceNotFoundException("Wallet with specified walletId not found");
        }

        Wallet wallet = retWallet.get();
        if (wallet.isLocked()) {
            throw new InvalidAccountStateException("Wallet is locked for credit.");
        }
        TransactionResponse transactionResponse = TransactionRequestMapper.mapFundsDataToTransaction(transactionRequest, walletId, CREDIT);
        try {
            createTransaction(transactionResponse, countryCode);
            transactionResponse = creditAccount(wallet, transactionResponse);
            updateStatus(transactionResponse);

            ApiResponse response = getTransactionResponse(transactionResponse);
            if (response.isSuccess()) {
                String firstName = !isEmpty(wallet.getWalletOwner().getFirstName()) ? capitalize(wallet.getWalletOwner().getFirstName()) : "Anonymous";
                String amount = NumberFormat.getCurrencyInstance().format(transactionRequest.getAmount());
                String email = wallet.getWalletOwner().getEmail();

                EmailRequest emailRequest = EmailRequest.builder().to(email).subject(SUCCESSFUL_CREDIT_TRANSACTION_SUBJECT)
                        .text(walletUtils.generateEmailText(firstName, amount, walletId, "credited to"))
                        .build();
                notificationApi.sendEmail(emailRequest);
            }
            return response;
        } catch (WalletException e) {
            log.error("Error crediting account: {}", e.getMessage());
            return fromException(e, transactionResponse);
        }
    }


    @Override
    public ApiResponse transferFund(CrossWalletRequest crossWalletRequest, String countryCode) {
        crossWalletRequest.setUniqueRef(walletUtils.generateTransactionRef());
        if (crossWalletRequest.getSourceAccountId().equals(crossWalletRequest.getTargetAccountId())) {
            throw new WalletException("Source and destination account cannot be the same", true);
        }
        Optional<Wallet> opSrWallet = walletRepository.getWalletByWalletIdAndCountry(crossWalletRequest.getSourceAccountId(), countryCode);
        Optional<Wallet> opTrWallet = walletRepository.getWalletByWalletIdAndCountry(crossWalletRequest.getTargetAccountId(), countryCode);

        if (opSrWallet.isPresent() && opTrWallet.isPresent()) {
            Wallet srWallet = opSrWallet.get();
            Wallet trWallet = opTrWallet.get();
            if (srWallet.isLocked()) {
                throw new InvalidAccountStateException("Sender wallet is locked for debit.");
            }

            if (trWallet.isLocked()) {
                throw new InvalidAccountStateException("Recipient's wallet is locked for credit.");
            }

            TransactionResponse transactionResponse = TransactionRequestMapper.mapFundsDataToTransaction(crossWalletRequest, crossWalletRequest.getSourceAccountId(), DEBIT);

            debitFunds(srWallet.getWalletId(), crossWalletRequest, countryCode);
            creditFunds(trWallet.getWalletId(), crossWalletRequest, countryCode);

            transactionResponse.setTargetAccountId(crossWalletRequest.getTargetAccountId());
            transactionResponse.setTransactionStatus(SUCCESS);
            transactionResponse.setBalance(srWallet.getBalance());

            return getTransactionResponse(transactionResponse);
        } else if (opSrWallet.isEmpty() && opTrWallet.isEmpty()) {
            throw new ResourceNotFoundException("Source Wallet and Target Wallet not found");
        } else if (opSrWallet.isEmpty()) {
            throw new ResourceNotFoundException("Source Wallet not found");
        } else {
            throw new ResourceNotFoundException("Target Wallet not found");
        }
    }

    public TransactionResponse debitAccount(Wallet wallet, TransactionResponse transactionResponse) {
        wallet.setBalance(wallet.getBalance() - transactionResponse.getAmount());
        Wallet saveWallet = walletRepository.save(wallet);
        transactionResponse.setBalance(saveWallet.getBalance());
        transactionResponse.setTransactionStatus(SUCCESS);
        return transactionResponse;
    }

    public TransactionResponse creditAccount(Wallet wallet, TransactionResponse transactionResponse) {
        wallet.setBalance(wallet.getBalance() + transactionResponse.getAmount());
        Wallet saveWallet = walletRepository.save(wallet);
        transactionResponse.setBalance(saveWallet.getBalance());
        transactionResponse.setTransactionStatus(SUCCESS);
        return transactionResponse;
    }


    public void checkSufficientFunds(Wallet wallet, TransactionRequest transactionRequest) {
        if (wallet.getBalance() < transactionRequest.getAmount()) {
            throw new InsufficientFundsException("Insufficient Funds");
        }
    }

    public void createTransaction(TransactionResponse transaction, String country) {
        Wallet walletEntity = walletRepository.findOneByWalletIdAndCountry(transaction.getSourceAccountId(), country);
        if (transactionRepository.countByUniqueRef(transaction.getUniqueRef()) > 0) {
            throw new DuplicateTransactionException("Transaction Reference Exists");
        }

        Transaction transactionEntity = TransactionConverter.convertToBDO.apply(transaction);
        transactionEntity.setWallet(walletEntity);
        transactionRepository.save(transactionEntity);

    }

    private void updateStatus(TransactionResponse transaction) {
        Assert.notNull(transaction, "Transaction cannot be null");
        Transaction transactionEntity = transactionRepository.getTransactionByUniqueRef(transaction.getUniqueRef());
        Assert.notNull(transactionEntity, "DB Transaction for ref: " + transaction.getUniqueRef() + " should not be null");

        if (transaction.getTransactionStatus() != transactionEntity.getTransactionStatus()) {

            transactionEntity.setErrorMessage(transaction.getError());
            transactionEntity.setTransactionStatus(transaction.getTransactionStatus()); // updating status to : SUCCESS/FAILED

            transactionRepository.save(transactionEntity);
        } else {
            log.warn("{} - No update was performed, transaction status ({}) is unchanged", transaction.getUniqueRef(), transaction.getTransactionStatus());
        }

    }


    public ApiResponse getTransactionResponse(TransactionResponse transaction) {
        if (transaction == null) {
            log.info("WALLET_COMPLETED_TRANSACTION {} {}", "null", "Successful Transaction");
            return ApiResponse.builder()
                    .message("Transaction not found")
                    .success(false)
                    .status(HttpStatus.NOT_FOUND.value())
                    .errorCode(ErrorCodes.NOT_FOUND_CODE)
                    .build();
        } else if (SUCCESS.equals(transaction.getTransactionStatus())) {
            log.info("WALLET_COMPLETED_TRANSACTION {} {}", transaction.getTransactionStatus(), "Successful Transaction");
            return ApiResponse.builder()
                    .success(true)
                    .data(transaction)
                    .message(Optional.of("Successful Transaction").orElse("Successful"))
                    .status(HttpStatus.CREATED.value())
                    .build();
        } else if (TransactionStatus.FAILED.equals(transaction.getTransactionStatus())) {
            log.info("WALLET_COMPLETED_TRANSACTION {} {}", transaction.getTransactionStatus(), "Failed Transaction");
            return ApiResponse.builder()
                    .success(false)
                    .data(transaction)
                    .message(Optional.of("Failed Transaction").orElse(transaction.getError()))
                    .status(HttpStatus.BAD_REQUEST.value())
                    .errorCode(ErrorCodes.FAILED_CODE)
                    .build();
        } else {
            throw new WalletException("Wallet Transaction still in progress");
        }
    }


    protected <T extends ApiResponse> T fromException(BaseException e, TransactionResponse txn) {

        log.error("{} - Error intercepted from transaction - {}, {}", txn.getSourceAccountId(), txn.getUniqueRef(), e);

        if (e instanceof DuplicateTransactionException || e instanceof InsufficientFundsException) {
            throw e;
        }

        ApiResponse response = null;
        if ((e instanceof FailedWalletOperationException)
                || (e instanceof ResourceNotFoundException)
                || e.isClientError()) {
            response = ApiResponse.builder()
                    .success(false)
                    .errorCode(e.getErrorCode())
                    .message(e.getMessage())
                    .data(txn)
                    .build();
        }

        //update transaction if available
        txn.setTransactionStatus(response != null ? TransactionStatus.FAILED : TransactionStatus.IN_PROGRESS);
        txn.setUniqueRef(walletUtils.generateTransactionRef());
        txn.setError(e.getMessage());

        Transaction transactionEntity = TransactionConverter.convertToBDO.apply(txn);
        transactionRepository.save(transactionEntity);

        updateStatus(txn);

        throw e;
    }


}
