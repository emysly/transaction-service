package com.reloadly.transactionservice.service.impl;

import com.reloadly.transactionservice.model.ApiUser;
import com.reloadly.transactionservice.repository.ApiUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserAuthDetailsServiceImpl implements UserDetailsService {

    private final ApiUserRepository apiUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApiUser apiUser = apiUserRepository.findByUserName(username);
        log.info("User: {}", apiUser);
        return new User(apiUser.getUserName(), apiUser.getPassword(), new ArrayList<>());
    }
}
