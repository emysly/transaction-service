package com.reloadly.transactionservice.service.impl;

import com.reloadly.transactionservice.exception.ResourceNotFoundException;
import com.reloadly.transactionservice.http.ApiResponse;
import com.reloadly.transactionservice.http.TransactionResponse;
import com.reloadly.transactionservice.http.WalletRequest;
import com.reloadly.transactionservice.http.WalletResponse;
import com.reloadly.transactionservice.model.Transaction;
import com.reloadly.transactionservice.model.Wallet;
import com.reloadly.transactionservice.model.WalletOwner;
import com.reloadly.transactionservice.model.enums.ActivityType;
import com.reloadly.transactionservice.repository.TransactionRepository;
import com.reloadly.transactionservice.repository.WalletOwnerRepository;
import com.reloadly.transactionservice.repository.WalletRepository;
import com.reloadly.transactionservice.service.WalletService;
import com.reloadly.transactionservice.service.utils.WalletUtils;
import com.reloadly.transactionservice.service.utils.converter.TransactionConverter;
import com.reloadly.transactionservice.service.utils.converter.WalletConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final TransactionRepository transactionRepository;
    private final WalletOwnerRepository walletOwnerRepository;
    private final WalletUtils walletUtils;

    @Override
    public ApiResponse createWallet(WalletRequest walletRequest, String countryCode) {
        Optional<Wallet> retWallet = walletRepository.getWalletByClientIdAndCountry(walletRequest.getClientId(), countryCode);
        Optional<WalletOwner> retWalletOwner = walletOwnerRepository.getWalletOwnerByClientId(walletRequest.getClientId());
        Wallet wallet = retWallet.orElse(new Wallet());
        WalletOwner walletOwner = retWalletOwner.orElse(new WalletOwner());
        WalletResponse walletResponse;
        if (retWallet.isPresent() && retWalletOwner.isPresent()) {
            walletResponse = WalletConverter.convertToEntity.apply(retWallet.get());
        } else {
            walletOwner.setClientId(walletRequest.getClientId());
            walletOwner.setEmail(walletRequest.getEmail());
            walletOwner.setFirstName(walletRequest.getFirstName());
            walletOwner.setMiddleName(walletRequest.getMiddleName());
            walletOwner.setLastName(walletRequest.getLastName());
            walletOwner.setFullName(walletRequest.getFirstName() + " " + walletRequest.getMiddleName() + " " + walletRequest.getLastName());
            walletOwner.setPhoneNumber(walletRequest.getPhoneNumber());
            walletOwnerRepository.save(walletOwner);

            wallet.setWalletId(walletUtils.generateWalletAccountId());
            wallet.setClientId(walletRequest.getClientId());
            wallet.setCountry(countryCode);
            wallet.setWalletOwner(walletOwner);
            walletRepository.save(wallet);
            walletResponse = WalletConverter.convertToEntity.apply(wallet);
        }
        return ApiResponse.builder().message("Wallet created successful").success(true).status(201).data(walletResponse).build();
    }

    @Override
    public ApiResponse getWallet(String walletId, String countryCode) {
        Optional<Wallet> retWallet = walletRepository.getWalletByWalletIdAndCountry(walletId, countryCode);
        if (retWallet.isEmpty()) {
            throw new ResourceNotFoundException("Wallet with specified walletId not found");
        }
        WalletResponse walletResponse = WalletConverter.convertToEntity.apply(retWallet.get());
        return ApiResponse.builder().message("Wallet fetched successful").success(true).status(200).data(walletResponse).build();
    }

    @Override
    public ApiResponse getWalletByClientId(String clientId, String countryCode) {
        Optional<Wallet> retWallet = walletRepository.getWalletByClientIdAndCountry(clientId, countryCode);
        if (retWallet.isEmpty()) {
            throw new ResourceNotFoundException("Wallet not found for clientId " + clientId);
        }
        WalletResponse walletResponse = WalletConverter.convertToEntity.apply(retWallet.get());
        return ApiResponse.builder().message("Wallet fetched successful").success(true).status(200).data(walletResponse).build();
    }

    @Override
    public ApiResponse getTransactions(String walletId, Date startDate, Date endDate, String countryCode) {
        Optional<Wallet> walletByWalletId = walletRepository.getWalletByWalletIdAndCountry(walletId, countryCode);
        if (walletByWalletId.isEmpty()) {
            throw new ResourceNotFoundException("Wallet with specified walletId not found");
        }
        List<Transaction> transactions = transactionRepository.findTransactionByWalletIdAndCreatedAtBetween(walletByWalletId.get().getId(), startDate, endDate);

        List<TransactionResponse> transactionResponses = new ArrayList<>();

        transactions.forEach(transaction -> {
            TransactionResponse transactionResponse = TransactionConverter.convertToEntity.apply(transaction);
            transactionResponses.add(transactionResponse);
        });

        return ApiResponse.builder().message("Fetched " + transactionResponses.size() + " transactions successful").status(200).success(true).data(transactionResponses).build();

    }

    @Override
    public ApiResponse lockWallet(String walletId, String reason, String countryCode) {
        Optional<Wallet> opWallet = walletRepository.getWalletByWalletIdAndCountry(walletId, countryCode);
        if (opWallet.isEmpty()) {
            throw new ResourceNotFoundException("Wallet with specified walletId not found");
        }
        opWallet.get().setLocked(true);
        opWallet.get().setReason(reason);
        opWallet.get().setLockType(ActivityType.LOCK);
        Wallet wallet = walletRepository.save(opWallet.get());
        WalletResponse walletResponse = WalletResponse.builder().walletId(walletId)
                .clientId(wallet.getClientId()).balance(wallet.getBalance()).locked(wallet.isLocked()).build();

        return ApiResponse.builder().message("Wallet locked").success(true).status(200).data(walletResponse).build();
    }

    @Override
    public ApiResponse unlockWallet(String walletId, String reason, String countryCode) {
        Optional<Wallet> opWallet = walletRepository.getWalletByWalletIdAndCountry(walletId, countryCode);
        if (opWallet.isEmpty()) {
            throw new ResourceNotFoundException("Wallet with specified walletId not found");
        }
        opWallet.get().setLocked(false);
        opWallet.get().setReason(reason);
        opWallet.get().setLockType(ActivityType.UNLOCK);
        Wallet wallet = walletRepository.save(opWallet.get());
        WalletResponse walletResponse = WalletResponse.builder().walletId(walletId)
                .clientId(wallet.getClientId()).balance(wallet.getBalance()).locked(wallet.isLocked()).build();

        return ApiResponse.builder().message("Wallet unlocked").success(true).status(200).data(walletResponse).build();
    }

}
