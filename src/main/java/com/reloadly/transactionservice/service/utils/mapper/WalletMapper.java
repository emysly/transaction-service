package com.reloadly.transactionservice.service.utils.mapper;

import com.reloadly.transactionservice.http.WalletResponse;
import com.reloadly.transactionservice.model.Wallet;
import com.reloadly.transactionservice.model.WalletOwner;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

public class WalletMapper extends CustomMapper<WalletResponse, Wallet> {

    @Override
    public void mapAtoB(WalletResponse wallet, Wallet walletEntity, MappingContext context) {
        walletEntity.setWalletId(wallet.getWalletId());
        WalletOwner walletOwner = new WalletOwner();
        walletOwner.setEmail(wallet.getEmail());
        walletOwner.setLastName(wallet.getLastName());
        walletOwner.setPhoneNumber(wallet.getPhoneNumber());
        walletOwner.setMiddleName(wallet.getMiddleName());
        walletOwner.setFullName(wallet.getFirstName() + " " + wallet.getMiddleName() + " " + wallet.getLastName());
        walletOwner.setClientId(wallet.getClientId());
        walletEntity.setWalletOwner(walletOwner);
        walletEntity.setCountry(wallet.getCountry());
        walletEntity.setLocked(wallet.isLocked());
        walletEntity.setClientId(wallet.getClientId());
    }

    @Override
    public void mapBtoA(Wallet walletEntity, WalletResponse wallet, MappingContext context) {
        wallet.setWalletId(walletEntity.getWalletId());
        wallet.setClientId(walletEntity.getClientId());
        wallet.setCountry(walletEntity.getCountry());
        wallet.setLocked(walletEntity.isLocked());
        wallet.setFirstName(walletEntity.getWalletOwner().getFirstName());
        wallet.setLastName(walletEntity.getWalletOwner().getLastName());
        wallet.setPhoneNumber(walletEntity.getWalletOwner().getPhoneNumber());
        wallet.setMiddleName(walletEntity.getWalletOwner().getMiddleName());
    }
}
