package com.reloadly.transactionservice.service.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;


@Component
public class WalletUtils {

    public String generateWalletAccountId() {
        return RandomStringUtils.randomNumeric(10);
    }

    public String generateTransactionRef() {
        return RandomStringUtils.randomAlphanumeric(10);
    }

  public String generateEmailText(String firstName, String amount, String walletId, String text) {
      return "Hi " + firstName + ", " + amount + " has been " + text + " your account with account number " + walletId;
  }
}
