package com.reloadly.transactionservice.service.utils.mapper;

import com.reloadly.transactionservice.http.TransactionResponse;
import com.reloadly.transactionservice.model.Transaction;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

import static com.reloadly.transactionservice.model.enums.TransactionType.CREDIT;

public class TransactionMapper extends CustomMapper<TransactionResponse, Transaction> {

    @Override
    public void mapAtoB(TransactionResponse transactionResponse, Transaction transactionEntity, MappingContext context) {
        long amount = Math.abs(transactionResponse.getAmount());
        if (transactionResponse.getTransactionType() == CREDIT) {
            transactionEntity.setAmount(amount);
        } else {
            transactionEntity.setAmount(-amount);
        }

        transactionEntity.setDescription(transactionResponse.getDescription());
        transactionEntity.setUniqueRef(transactionResponse.getUniqueRef());
        transactionEntity.setTransactionType(transactionResponse.getTransactionType());
        transactionEntity.setEntryDate(transactionResponse.getEntryDate());
        transactionEntity.setErrorMessage(transactionResponse.getError());
    }

    @Override
    public void mapBtoA(Transaction transactionEntity, TransactionResponse transaction, MappingContext context) {
        transaction.setSourceAccountId(transactionEntity.getWallet().getWalletId());
        transaction.setAmount(Math.abs(transactionEntity.getAmount()));
        transaction.setDescription(transactionEntity.getDescription());
        transaction.setUniqueRef(transactionEntity.getUniqueRef());
        transaction.setTransactionType(transactionEntity.getTransactionType());
        transaction.setError(transactionEntity.getErrorMessage());
        
        transaction.setEntryDate(transactionEntity.getEntryDate());
        transaction.setTransactionStatus(transactionEntity.getTransactionStatus());
    }
}
