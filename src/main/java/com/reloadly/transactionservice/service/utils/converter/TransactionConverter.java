package com.reloadly.transactionservice.service.utils.converter;

import com.reloadly.transactionservice.http.TransactionResponse;
import com.reloadly.transactionservice.model.Transaction;
import com.reloadly.transactionservice.service.utils.mapper.TransactionMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import java.util.function.Function;


public class TransactionConverter {

        private static final MapperFacade mapperFacade;

    static {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(TransactionResponse.class, Transaction.class)
                .customize(new TransactionMapper())
                .register();
        mapperFacade = factory.getMapperFacade();
    }

    final public static Function<Transaction, TransactionResponse> convertToEntity = transactionEntity -> mapperFacade.map(transactionEntity, TransactionResponse.class);
    final public static Function<TransactionResponse, Transaction> convertToBDO = transactionResponse -> mapperFacade.map(transactionResponse, Transaction.class);
}