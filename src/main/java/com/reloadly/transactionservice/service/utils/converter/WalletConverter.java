package com.reloadly.transactionservice.service.utils.converter;

import com.reloadly.transactionservice.http.WalletResponse;
import com.reloadly.transactionservice.model.Wallet;
import com.reloadly.transactionservice.service.utils.mapper.WalletMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

import java.util.function.Function;

public class WalletConverter {

    private final static MapperFacade mapperFacade;

    public static Function<Wallet, WalletResponse> convertToEntity;
    public static Function<WalletResponse, Wallet> convertToBDO;

    static {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(WalletResponse.class, Wallet.class)
            .customize(new WalletMapper())
            .byDefault()
            .register();
        mapperFacade = factory.getMapperFacade();

        convertToEntity = wallet -> mapperFacade.map(wallet, WalletResponse.class);
        convertToBDO = walletResponse -> mapperFacade.map(walletResponse, Wallet.class);
    }

}
