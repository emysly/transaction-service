package com.reloadly.transactionservice.service.utils;

import com.reloadly.transactionservice.http.TransactionRequest;
import com.reloadly.transactionservice.http.TransactionResponse;
import com.reloadly.transactionservice.model.enums.TransactionType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@RequiredArgsConstructor
public class TransactionRequestMapper {

    public static TransactionResponse mapFundsDataToTransaction(TransactionRequest transactionRequest, String walletId, TransactionType transactionType) {
        return TransactionResponse.builder().
                uniqueRef(transactionRequest.getUniqueRef())
                .amount(Math.abs(transactionRequest.getAmount()))
                .description(transactionRequest.getDescription())
                .sourceAccountId(walletId)
                .transactionType(transactionType)
                .entryDate(new Date()).build();
    }
}
