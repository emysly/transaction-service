package com.reloadly.transactionservice.service;

import com.reloadly.transactionservice.http.*;

import java.util.Date;

public interface WalletService {
    ApiResponse createWallet(WalletRequest walletRequest, String countryCode);

    ApiResponse getWallet(String walletId, String countryCode);

    ApiResponse getWalletByClientId(String clientId, String countryCode);

    ApiResponse getTransactions(String walletId, Date startDate, Date endDate, String countryCode);

    ApiResponse lockWallet(String walletId, String reason, String countryCode);

    ApiResponse unlockWallet(String walletId, String reason, String countryCode);
}
