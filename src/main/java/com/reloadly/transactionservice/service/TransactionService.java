package com.reloadly.transactionservice.service;

import com.reloadly.transactionservice.http.*;

import java.util.List;

public interface TransactionService {
    ApiResponse transferFund(CrossWalletRequest crossWalletRequest, String countryCode);
    ApiResponse debitFunds(String walletId, TransactionRequest transactionRequest, String countryCode);
    ApiResponse creditFunds(String walletId, TransactionRequest transactionRequest, String countryCode);
}
