package com.reloadly.transactionservice.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;

import static springfox.documentation.builders.PathSelectors.*;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
public class SwaggerConfig {

    @Bean
    @ConditionalOnProperty(name = "swagger.enabled", matchIfMissing = true)
    public Docket api() {
        return new Docket(SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.reloadly.transactionservice"))
                .paths(regex("/api/v1.*"))
                .build()
                .apiInfo(appInfo());
    }

    private ApiInfo appInfo() {
        Contact contact = new Contact("Reloadly Engineering", "https://www.reloadly.com", "developers@reloadly.com");
        return new ApiInfoBuilder()
                .title("Transaction service")
                .description("This micro service is in charge of managing the accounts.")
                .contact(contact)
                .license("Proprietary License")
                .version("1.0-beta")
                .build();
    }
}