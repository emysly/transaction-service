package com.reloadly.transactionservice;

import com.reloadly.transactionservice.model.ApiUser;
import com.reloadly.transactionservice.repository.ApiUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableFeignClients
@RequiredArgsConstructor
public class TransactionServiceApplication {

    private final ApiUserRepository apiUserRepository;

    @PostConstruct
    public void saveApiUser() {
        ApiUser apiUser = new ApiUser("admin", "admin");
        apiUserRepository.save(apiUser);
    }

    public static void main(String[] args) {
        SpringApplication.run(TransactionServiceApplication.class, args);
    }

}
