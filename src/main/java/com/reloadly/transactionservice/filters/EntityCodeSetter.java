package com.reloadly.transactionservice.filters;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
public class EntityCodeSetter extends OncePerRequestFilter {

    public static final String ENTITY_KEY = "X_ENTITY_CODE";

    public static final int SCOPE_ID = 0;

    @Value("${app.defaultCountryCode:NG}")
    private String defaultCountryCode;

    private String getParamFromQueryString(String queryString) {
        if (queryString == null || queryString.isEmpty()) {
            return null;
        }
        Map<String, String> queryParams = new HashMap<>();
        String[] pairs = queryString.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            queryParams.put(URLDecoder.decode(pair.substring(0, idx), StandardCharsets.UTF_8), URLDecoder.decode(pair.substring(idx + 1), StandardCharsets.UTF_8));
        }
        return queryParams.get("entity");
    }

    @Override
    protected void doFilterInternal(HttpServletRequest hsr, HttpServletResponse hsr1, FilterChain fc) throws ServletException, IOException {
        String countryCode = hsr.getHeader("X-Entity");

        if (countryCode == null || countryCode.isEmpty()) {
            countryCode = getParamFromQueryString(hsr.getQueryString());
            countryCode = countryCode == null || countryCode.isEmpty() ? defaultCountryCode : countryCode;
        }

        Objects.requireNonNull(RequestContextHolder.getRequestAttributes()).setAttribute(ENTITY_KEY, countryCode.toUpperCase(), SCOPE_ID);

        fc.doFilter(hsr, hsr1);
    }

    public static String getRequestEntity() {
        return (String) RequestContextHolder.currentRequestAttributes().getAttribute(ENTITY_KEY, SCOPE_ID);
    }
}
