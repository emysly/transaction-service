package com.reloadly.transactionservice.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reloadly.transactionservice.service.impl.UserAuthDetailsServiceImpl;
import com.reloadly.transactionservice.service.utils.jwt.JwtUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtFilter extends OncePerRequestFilter {

    private final JwtUtil jwtUtil;

    private final UserAuthDetailsServiceImpl userAuthDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            if (request.getServletPath().equals("/api/v1/auth")) {
                filterChain.doFilter(request, response);
            } else {
                String authorizationHeader = request.getHeader("Authorization");
                log.info("Auth: {}", authorizationHeader);
                if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {

                    String token = authorizationHeader.substring(7);
                    String username = jwtUtil.extractUsername(token);
                    log.info("Username: {}", username);
                    if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                        UserDetails userDetails = userAuthDetailsService.loadUserByUsername(username);
                        if (jwtUtil.validateToken(token, userDetails)) {
                            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                                    = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());

                            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                        }
                    }
                }
                filterChain.doFilter(request, response);
            }
        } catch (Exception ex) {
            response.setStatus(403);
            response.setContentType("application/json;charset=UTF-8");
            Map<String, Object> objectMap = new HashMap<>();
            objectMap.put("timestamp", new Date());
            objectMap.put("status", 403);
            objectMap.put("message", "Access Denied");
            objectMap.put("success", false);
            objectMap.put("errorCode", "Forbidden");
            ObjectMapper objectMapper = new ObjectMapper();
            response.getWriter().write(objectMapper.writeValueAsString(objectMap));
        }
    }
}
