package com.reloadly.transactionservice.http;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.reloadly.transactionservice.model.WalletOwner;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WalletResponse implements Serializable {

    @NotEmpty
    private String walletId;

    @NotEmpty
    private String clientId;

    private String sourceId;

    @NotEmpty
    private String phoneNumber;

    @NotEmpty
    private String email;

    @NotEmpty
    private String firstName;

    private String middleName;

    @NotEmpty
    private String lastName;

    @NotEmpty
    private String fullName;

    @NotEmpty
    private long balance;

    @JsonIgnore
    String country;

    @NotEmpty
    @Builder.Default
    boolean locked = false;

}
