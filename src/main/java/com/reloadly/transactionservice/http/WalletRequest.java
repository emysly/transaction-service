package com.reloadly.transactionservice.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WalletRequest implements Serializable {

    @NotEmpty
    private String clientId;

    @NotEmpty
    private String phoneNumber;

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    private String firstName;

    private String middleName;

    @NotEmpty
    private String lastName;

}
