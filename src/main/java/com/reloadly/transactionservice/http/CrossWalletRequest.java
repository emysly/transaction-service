package com.reloadly.transactionservice.http;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class CrossWalletRequest extends TransactionRequest {
    @NotBlank
    private String sourceAccountId;

    @NotNull
    private String targetAccountId;

}
