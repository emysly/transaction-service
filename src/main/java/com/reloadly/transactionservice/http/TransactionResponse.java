package com.reloadly.transactionservice.http;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.reloadly.transactionservice.model.enums.TransactionStatus;
import com.reloadly.transactionservice.model.enums.TransactionType;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionResponse {

    private String targetAccountId;

    @NotEmpty
    private String sourceAccountId;

    @NotEmpty
    private String uniqueRef;

    @NotEmpty
    private Long amount;

    @NotEmpty
    private String description;

    @NotEmpty
    private TransactionType transactionType;

    private TransactionStatus transactionStatus;

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date entryDate;

    private Long balance;

    private String error;

}
