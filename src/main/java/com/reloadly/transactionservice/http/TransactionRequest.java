package com.reloadly.transactionservice.http;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class TransactionRequest {

    private String uniqueRef;

    @NotNull
    @Min(1)
    private long amount;

    @NotBlank
    private String description;
}