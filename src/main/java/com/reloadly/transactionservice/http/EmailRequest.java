package com.reloadly.transactionservice.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmailRequest implements Serializable {
    @NotEmpty
    @Email
    private String to;
    @NotEmpty
    private String text;
    @NotEmpty
    private String subject;
}