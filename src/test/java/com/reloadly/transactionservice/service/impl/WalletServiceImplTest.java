package com.reloadly.transactionservice.service.impl;

import com.reloadly.transactionservice.exception.ResourceNotFoundException;
import com.reloadly.transactionservice.http.ApiResponse;
import com.reloadly.transactionservice.http.WalletRequest;
import com.reloadly.transactionservice.model.Transaction;
import com.reloadly.transactionservice.model.Wallet;
import com.reloadly.transactionservice.model.WalletOwner;
import com.reloadly.transactionservice.repository.TransactionRepository;
import com.reloadly.transactionservice.repository.WalletOwnerRepository;
import com.reloadly.transactionservice.repository.WalletRepository;
import com.reloadly.transactionservice.service.utils.WalletUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@Slf4j
class WalletServiceImplTest {

    private static final String CLIENT_ID = "11111111";
    private static final String COUNTRY_CODE = "NG";
    private static final String WALLET_ID = "1111111111";

    @Mock
    private WalletRepository walletRepository;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private WalletOwnerRepository walletOwnerRepository;

    @Mock
    private WalletUtils walletUtils;

    @InjectMocks
    private WalletServiceImpl instance;

    WalletRequest walletRequest;

    Wallet wallet;

    WalletOwner walletOwner;

    @BeforeEach
    void setUp() {
        initMocks(this);
        WalletRequest walletRequest = WalletRequest.builder().clientId("11111111").email("test@gmail.com").firstName("test")
                .middleName("test").lastName("test").phoneNumber("08011111111").build();

        this.walletRequest = walletRequest;

        WalletOwner walletOwner = new WalletOwner();
        walletOwner.setClientId(walletRequest.getClientId());
        walletOwner.setEmail(walletRequest.getEmail());
        walletOwner.setFirstName(walletRequest.getFirstName());
        walletOwner.setMiddleName(walletRequest.getMiddleName());
        walletOwner.setLastName(walletRequest.getLastName());
        walletOwner.setFullName(walletRequest.getFirstName() + " " + walletRequest.getMiddleName() + " " + walletRequest.getLastName());
        walletOwner.setPhoneNumber(walletRequest.getPhoneNumber());
        this.walletOwner = walletOwner;


        Wallet wallet = new Wallet();
        wallet.setWalletId(WALLET_ID);
        wallet.setClientId(CLIENT_ID);
        wallet.setCountry(COUNTRY_CODE);
        wallet.setBalance(20000L);
        wallet.setWalletOwner(walletOwner);
        this.wallet = wallet;
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createNewWallet() {
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);

        ApiResponse response = instance.createWallet(this.walletRequest, COUNTRY_CODE);
        assertTrue(response.isSuccess());
        assertNotNull(response.getData());
    }

    @Test
    void createWalletIfWalletAlreadyExist() {
        when(walletRepository.getWalletByClientIdAndCountry("11111111", "NG")).thenReturn(Optional.of(this.wallet));
        when(walletOwnerRepository.getWalletOwnerByClientId("11111111")).thenReturn(Optional.of(this.walletOwner));

        ApiResponse response = instance.createWallet(this.walletRequest, "NG");

        assertTrue(response.isSuccess());
        assertEquals(201, response.getStatus());
        assertEquals("Wallet created successful", response.getMessage());
        assertNotNull(response.getData());
    }


    @Test
    void getWalletByWalletIdDoesNotExists() {
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.getWallet("111111111", "NG");
        });

        assertEquals(404, exception.getStatus());
        assertEquals("Wallet with specified walletId not found", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());

    }

    @Test
    void getWallet() {

        when(walletRepository.getWalletByWalletIdAndCountry("1111111111", "NG")).thenReturn(Optional.ofNullable(wallet));
        ApiResponse response = instance.getWallet("1111111111", "NG");

        assertTrue(response.isSuccess());
        assertEquals(200, response.getStatus());
        assertEquals("Wallet fetched successful", response.getMessage());
        assertNotNull(response.getData());
    }

    @Test
    void getWalletByClientIdThatDoesNotExists() {
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.getWalletByClientId("11111111", "NG");
        });

        assertEquals(404, exception.getStatus());
        assertEquals("Wallet not found for clientId 11111111", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());
    }

    @Test
    void getWalletByClientId() {

        when(walletRepository.getWalletByClientIdAndCountry("11111111", "NG")).thenReturn(Optional.ofNullable(wallet));
        ApiResponse response = instance.getWalletByClientId("11111111", "NG");

        assertTrue(response.isSuccess());
        assertEquals(200, response.getStatus());
        assertEquals("Wallet fetched successful", response.getMessage());
        assertNotNull(response.getData());
    }

    @Test
    void getTransactionsByWalletIdThatDoesNotExists() {

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.getTransactions("1111111", new Date(), new Date(), COUNTRY_CODE);
        });

        assertEquals(404, exception.getStatus());
        assertEquals("Wallet with specified walletId not found", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());
    }

    @Test
    void getWalletTransactions() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.ofNullable(this.wallet));
        when(transactionRepository.findTransactionByWalletIdAndCreatedAtBetween(wallet.getId(), new Date(), new Date())).thenReturn(List.of(new Transaction()));


        ApiResponse response = instance.getTransactions(WALLET_ID, new Date(), new Date(), COUNTRY_CODE);

        assertTrue(response.isSuccess());
        assertEquals(200, response.getStatus());
        assertNotNull(response.getData());
    }

    @Test
    void lockWalletByWalletIdThatDoesNotExists() {

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.lockWallet("1111111", "reason", COUNTRY_CODE);
        });

        assertEquals(404, exception.getStatus());
        assertEquals("Wallet with specified walletId not found", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());
    }

    @Test
    void lockWallet() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.ofNullable(this.wallet));
        this.wallet.setLocked(true);
        log.info("Wallet: {}", this.wallet);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);

        ApiResponse response = instance.lockWallet(WALLET_ID, "reason", COUNTRY_CODE);

        assertTrue(response.isSuccess());
        assertEquals(200, response.getStatus());
        assertEquals("Wallet locked", response.getMessage());
        assertNotNull(response.getData());
    }

    @Test
    void unlockWalletByWalletIdThatDoesNotExists() {

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.unlockWallet("1111111", "reason", COUNTRY_CODE);
        });

        assertEquals(404, exception.getStatus());
        assertEquals("Wallet with specified walletId not found", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());
    }

    @Test
    void unlockWallet() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.ofNullable(this.wallet));
        this.wallet.setLocked(false);
        this.wallet.setWalletId("1111111111");
        log.info("Wallet: {}", this.wallet);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);

        ApiResponse response = instance.unlockWallet(WALLET_ID, "reason", COUNTRY_CODE);

        assertTrue(response.isSuccess());
        assertEquals(200, response.getStatus());
        assertEquals("Wallet unlocked", response.getMessage());
        assertNotNull(response.getData());
    }
}