package com.reloadly.transactionservice.service.impl;

import com.reloadly.transactionservice.api.NotificationApi;
import com.reloadly.transactionservice.exception.*;
import com.reloadly.transactionservice.http.*;
import com.reloadly.transactionservice.model.Transaction;
import com.reloadly.transactionservice.model.Wallet;
import com.reloadly.transactionservice.model.WalletOwner;
import com.reloadly.transactionservice.model.enums.TransactionStatus;
import com.reloadly.transactionservice.repository.TransactionRepository;
import com.reloadly.transactionservice.repository.WalletRepository;
import com.reloadly.transactionservice.service.utils.WalletUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static com.reloadly.transactionservice.model.enums.TransactionStatus.SUCCESS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@Slf4j
class TransactionServiceImplTest {

    private static final String CLIENT_ID = "11111111";
    private static final String COUNTRY_CODE = "NG";
    private static final String WALLET_ID = "1111111111";
    private static final String TR_WALLET_ID = "2222222222";

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private WalletRepository walletRepository;

    @Mock
    private WalletUtils walletUtils;

    @Mock
    private NotificationApi notificationApi;

    @InjectMocks
    private TransactionServiceImpl instance;

    TransactionRequest request;
    CrossWalletRequest crossWalletRequest;
    Wallet wallet;
    WalletOwner walletOwner;
    Transaction transaction;
    TransactionResponse transactionResponse;
    Wallet trWallet;

    @BeforeEach
    void setUp() {
        initMocks(this);
        request = new TransactionRequest();
        request.setDescription("description");
        request.setUniqueRef("uniqueRef");
        request.setAmount(100000);

        crossWalletRequest = new CrossWalletRequest();
        crossWalletRequest.setSourceAccountId(WALLET_ID);
        crossWalletRequest.setTargetAccountId(TR_WALLET_ID);

        WalletRequest walletRequest = WalletRequest.builder().clientId("11111111").email("test@gmail.com").firstName("test")
                .middleName("test").lastName("test").phoneNumber("08011111111").build();

        walletOwner = new WalletOwner();
        walletOwner.setClientId(walletRequest.getClientId());
        walletOwner.setEmail(walletRequest.getEmail());
        walletOwner.setFirstName(walletRequest.getFirstName());
        walletOwner.setMiddleName(walletRequest.getMiddleName());
        walletOwner.setLastName(walletRequest.getLastName());
        walletOwner.setFullName(walletRequest.getFirstName() + " " + walletRequest.getMiddleName() + " " + walletRequest.getLastName());
        walletOwner.setPhoneNumber(walletRequest.getPhoneNumber());


        wallet = new Wallet();
        wallet.setWalletId(WALLET_ID);
        wallet.setClientId(CLIENT_ID);
        wallet.setCountry(COUNTRY_CODE);
        wallet.setBalance(20000L);
        wallet.setWalletOwner(walletOwner);

        transaction = new Transaction();
        transaction.setAmount(200L);
        transaction.setUniqueRef("uniqueRef");
        transaction.setTransactionStatus(SUCCESS);

        transactionResponse = TransactionResponse.builder()
                .amount(200L)
                .uniqueRef("uniqueRef")
                .transactionStatus(SUCCESS)
                .build();

        trWallet = new Wallet();
        wallet.setWalletId(TR_WALLET_ID);
        wallet.setClientId(CLIENT_ID);
        wallet.setCountry(COUNTRY_CODE);
        wallet.setBalance(20000L);
        wallet.setWalletOwner(walletOwner);
    }


    @Test
    void debitFundsByWalletIdThatDoesNotExists() {

        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.empty());
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.debitFunds("111111111", request,"NG");
        });

        assertEquals(404, exception.getStatus());
        assertEquals("Wallet with specified walletId not found", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());
    }

    @Test
    void debitFundsByWalletIdWhenWalletIsLocked() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        this.wallet.setLocked(true);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);
        InvalidAccountStateException exception = assertThrows(InvalidAccountStateException.class, () -> {
            instance.debitFunds("1111111111", request,"NG");
        });

        assertEquals("Wallet is locked for debit.", exception.getMessage());
        assertEquals("E01", exception.getErrorCode());
    }

    @Test
    void debitFundsByWalletIdWhenWalletHasInsufficientFunds() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        this.wallet.setBalance(0L);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);

        InsufficientFundsException exception = assertThrows(InsufficientFundsException.class, () -> {
            instance.checkSufficientFunds(this.wallet, request);
        });

        assertEquals(400, exception.getStatus());
        assertEquals("Insufficient Funds", exception.getMessage());
        assertEquals("E02", exception.getErrorCode());
    }

    @Test
    void createTransactionWhenTransactionUniqueRefAlreadyExists() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
       when(transactionRepository.countByUniqueRef(this.transaction.getUniqueRef())).thenReturn(1);

        DuplicateTransactionException exception = assertThrows(DuplicateTransactionException.class, () -> {
            instance.createTransaction(transactionResponse, COUNTRY_CODE);
        });

        assertEquals(400, exception.getStatus());
        assertEquals("Transaction Reference Exists", exception.getMessage());
        assertEquals("E03", exception.getErrorCode());
    }

    @Test
    void createTransaction() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        when(transactionRepository.countByUniqueRef(this.transaction.getUniqueRef())).thenReturn(0);

        this.transaction.setWallet(this.wallet);
        when(transactionRepository.save(this.transaction)).thenReturn(this.transaction);

        assertEquals(this.transaction.getWallet(), this.wallet);
    }

    @Test
    void debitAccount() {
        when(walletRepository.findOneByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(this.wallet);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);
        TransactionResponse response = instance.debitAccount(this.wallet, transactionResponse);

        assertEquals(19800, response.getBalance());
        assertEquals(SUCCESS, response.getTransactionStatus());
    }

    @Test
    void creditAccount() {
        when(walletRepository.findOneByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(this.wallet);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);
        TransactionResponse response = instance.creditAccount(this.wallet, transactionResponse);

        assertEquals(20200, response.getBalance());
        assertEquals(SUCCESS, response.getTransactionStatus());
    }

    @Test
    void creditFundsByWalletIdThatDoesNotExists() {

        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.empty());
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.creditFunds("111111111", request,"NG");
        });

        assertEquals(404, exception.getStatus());
        assertEquals("Wallet with specified walletId not found", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());
    }

    @Test
    void creditFundsByWalletIdWhenWalletIsLocked() {
        when(walletRepository.getWalletByWalletIdAndCountry(WALLET_ID, COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        this.wallet.setLocked(true);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);
        InvalidAccountStateException exception = assertThrows(InvalidAccountStateException.class, () -> {
            instance.creditFunds(WALLET_ID, request,COUNTRY_CODE);
        });

        assertEquals(500, exception.getStatus());
        assertEquals("Wallet is locked for credit.", exception.getMessage());
        assertEquals("E01", exception.getErrorCode());
    }

    @Test
    void transferFundsWhenSrWalletIdIsEqualToTrWalletId() {
        crossWalletRequest.setTargetAccountId(WALLET_ID);
       WalletException exception = assertThrows(WalletException.class, () -> {
            instance.transferFund(crossWalletRequest, COUNTRY_CODE);
        });

        assertTrue(exception.isClientError());
        assertEquals(400, exception.getStatus());
        assertEquals("Source and destination account cannot be the same", exception.getMessage());
    }

    @Test
    void transferFundsWhenSrWalletAndTrWalletDoesNotExists() {
        when(walletRepository.getWalletByWalletIdAndCountry(crossWalletRequest.getSourceAccountId(), COUNTRY_CODE)).thenReturn(Optional.empty());
        when(walletRepository.getWalletByWalletIdAndCountry(crossWalletRequest.getTargetAccountId(), COUNTRY_CODE)).thenReturn(Optional.empty());
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.transferFund(crossWalletRequest, COUNTRY_CODE);
        });

        assertEquals(404, exception.getStatus());
        assertEquals("NOT_FOUND", exception.getErrorCode());
        assertEquals("Source Wallet and Target Wallet not found", exception.getMessage());
    }

    @Test
    void transferFundsWhenSrWalletDoesNotExists() {
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getSourceAccountId(), COUNTRY_CODE)).thenReturn(Optional.empty());
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getTargetAccountId(), COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.transferFund(crossWalletRequest, COUNTRY_CODE);
        });

        assertEquals(404, exception.getStatus());
        assertEquals("NOT_FOUND", exception.getErrorCode());
        assertEquals("Source Wallet not found", exception.getMessage());
    }

    @Test
    void transferFundsWhenTrWalletDoesNotExists() {
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getSourceAccountId(), COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getTargetAccountId(), COUNTRY_CODE)).thenReturn(Optional.empty());
        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            instance.transferFund(crossWalletRequest, COUNTRY_CODE);
        });

        assertEquals(404, exception.getStatus());
        assertEquals("NOT_FOUND", exception.getErrorCode());
        assertEquals("Target Wallet not found", exception.getMessage());
    }

    @Test
    void transferFundsWhenSrWalletIsLocked() {
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getSourceAccountId(), COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getTargetAccountId(), COUNTRY_CODE)).thenReturn(Optional.of(this.trWallet));
        this.wallet.setLocked(true);
        when(walletRepository.save(this.wallet)).thenReturn(this.wallet);
        InvalidAccountStateException exception = assertThrows(InvalidAccountStateException.class, () -> {
            instance.transferFund(crossWalletRequest, COUNTRY_CODE);
        });

        assertEquals(500, exception.getStatus());
        assertEquals("E01", exception.getErrorCode());
        assertEquals("Sender wallet is locked for debit.", exception.getMessage());
    }

    @Test
    void transferFundsWhenTrWalletIsLocked() {
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getSourceAccountId(), COUNTRY_CODE)).thenReturn(Optional.of(this.wallet));
        when(walletRepository.getWalletByWalletIdAndCountry(this.crossWalletRequest.getTargetAccountId(), COUNTRY_CODE)).thenReturn(Optional.of(this.trWallet));
        this.trWallet.setLocked(true);
        when(walletRepository.save(this.trWallet)).thenReturn(this.trWallet);
        InvalidAccountStateException exception = assertThrows(InvalidAccountStateException.class, () -> {
            instance.transferFund(crossWalletRequest, COUNTRY_CODE);
        });

        assertEquals(500, exception.getStatus());
        assertEquals("E01", exception.getErrorCode());
        assertEquals("Recipient's wallet is locked for credit.", exception.getMessage());
    }

    @Test
    void getTransactionResponseWhenTransactionResponseIsNull() {
        ApiResponse response = instance.getTransactionResponse(null);

        assertFalse(response.isSuccess());
        assertEquals(404, response.getStatus());
        assertEquals("NOT_FOUND", response.getErrorCode());
        assertEquals("Transaction not found", response.getMessage());
    }

    @Test
    void getTransactionResponseWhenTransactionResponseStatusIsSuccess() {
        TransactionResponse transactionResponse = TransactionResponse.builder().transactionStatus(SUCCESS).build();
        ApiResponse response = instance.getTransactionResponse(transactionResponse);

        assertTrue(response.isSuccess());
        assertEquals(201, response.getStatus());
        assertEquals("Successful Transaction", response.getMessage());
    }

    @Test
    void getTransactionResponseWhenTransactionResponseStatusIsFailed() {
        TransactionResponse transactionResponse = TransactionResponse.builder().transactionStatus(TransactionStatus.FAILED).build();
        ApiResponse response = instance.getTransactionResponse(transactionResponse);

        assertFalse(response.isSuccess());
        assertEquals(400, response.getStatus());
        assertEquals("Failed Transaction", response.getMessage());
    }
}